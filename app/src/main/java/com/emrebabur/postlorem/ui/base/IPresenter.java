package com.emrebabur.postlorem.ui.base;

/**
 * Created by emrebabur on 09/09/2017.
 */

public interface IPresenter<VIEW extends IView> {

    void subscribe(VIEW view, String... args);
    void unsubscribe();
}
