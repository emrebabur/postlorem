package com.emrebabur.postlorem.ui.postlist;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.emrebabur.postlorem.PostloremApp;
import com.emrebabur.postlorem.di.component.DaggerPresentationComponent;
import com.emrebabur.postlorem.di.module.AdapterModule;
import com.emrebabur.postlorem.di.module.LayoutManagerModule;
import com.emrebabur.postlorem.di.module.PresenterModule;
import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.ui.adapter.PostPreviewAdapter;
import com.emrebabur.postlorem.R;
import com.emrebabur.postlorem.ui.postdetail.PostDetailActivity;
import com.emrebabur.postlorem.util.IntentConst;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostListActivity extends AppCompatActivity implements IPostListContract.IPostListView, PostPreviewAdapter.IListener {

    private final String TAG = this.getClass().getSimpleName();

    public final String RECYCLER_STATE_KEY = "recycler_state";
    Parcelable recyclerState;

    @Inject IPostListContract.IPostListPresenter presenter;
    List<PostPreview> postPreviewList;

    @Inject PostPreviewAdapter postPreviewAdapter;
    @Inject @Named("vertical") LinearLayoutManager linearLayoutManager;

    @BindView(R.id.Toolbar)
    Toolbar toolbar;

    @BindView(R.id.PostRecyclerView)
    RecyclerView postPreviewRecyclerView;

    @BindView(R.id.LoadingView)
    ProgressBar loadingView;

    Pair<View, String>[] transitionPairs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);
        ButterKnife.bind(this);

        DaggerPresentationComponent.builder()
                .appComponent(((PostloremApp) getApplication()).getAppComponent())
                .presenterModule(new PresenterModule())
                .adapterModule(new AdapterModule())
                .layoutManagerModule(new LayoutManagerModule(this))
                .build()
                .inject(this);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle(R.string.app_name);

        postPreviewList = new ArrayList<>();
        postPreviewAdapter.setListener(this);
        postPreviewAdapter.setModel(postPreviewList);
        postPreviewRecyclerView.setLayoutManager(linearLayoutManager);
        postPreviewRecyclerView.setAdapter(postPreviewAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.subscribe(this);
    }

    @Override
    protected void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        recyclerState = linearLayoutManager.onSaveInstanceState();
        outState.putParcelable(RECYCLER_STATE_KEY, recyclerState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null)
            recyclerState = savedInstanceState.getParcelable(RECYCLER_STATE_KEY);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showPosts(List<PostPreview> postPreviewList) {
        this.postPreviewList.clear();
        this.postPreviewList.addAll(postPreviewList);
        postPreviewAdapter.notifyDataSetChanged();

        if(recyclerState != null) {
            postPreviewRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerState);
        }
    }

    @Override
    public void showError() {

    }

    @Override
    public void onItemSelected(String postId, Pair<View, String>... transitionPairs) {
        this.transitionPairs = transitionPairs;
        presenter.onPostClick(postId);
    }

    @Override
    public void openPostDetail(String postId) {
        Intent intent = new Intent(this, PostDetailActivity.class);
        intent.putExtra(IntentConst.KEY_POST_ID, postId);
        intent.putExtra(IntentConst.KEY_AVATAR_IMAGE_TRANSITION_NAME, transitionPairs[0].second);
        intent.putExtra(IntentConst.KEY_TITLE_TRANSITION_NAME, transitionPairs[1].second);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, transitionPairs);
        startActivity(intent, options.toBundle());
        //startActivity(intent);
    }
}
