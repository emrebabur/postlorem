package com.emrebabur.postlorem.ui.postlist;

import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.ui.base.IPresenter;
import com.emrebabur.postlorem.ui.base.IView;

import java.util.List;

/**
 * Created by emrebabur on 09/09/2017.
 */

public interface IPostListContract {

    interface IPostListView extends IView {
        void showPosts(List<PostPreview> posts);
        void openPostDetail(String postId);
    }

    interface IPostListPresenter extends IPresenter<IPostListView> {
        void onPostClick(String postId);
    }
}
