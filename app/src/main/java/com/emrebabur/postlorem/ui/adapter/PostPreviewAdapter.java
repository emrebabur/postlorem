package com.emrebabur.postlorem.ui.adapter;

import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emrebabur.postlorem.R;
import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.util.GlideApp;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emrebabur on 09/09/2017.
 */

public class PostPreviewAdapter extends RecyclerView.Adapter<PostPreviewAdapter.PostViewHolder> {

    private final String TAG = this.getClass().getSimpleName();
    private List<PostPreview> postPreviewList;

    public interface IListener {
        void onItemSelected(String postId, Pair<View, String>... transitionPairs);
    }

    private IListener listener;

    public PostPreviewAdapter() {
    }

    public void setModel(List<PostPreview> postPreviewList) {
        this.postPreviewList = postPreviewList;
        notifyDataSetChanged();
    }

    public void setListener(IListener listener) {
        this.listener = listener;
    }

    @Override
    public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.postcard, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostViewHolder holder, int position) {
        PostPreview postPreview = postPreviewList.get(position);
        holder.bind(postPreview, listener);
    }

    @Override
    public int getItemCount() {
        return postPreviewList.size();
    }

    static class PostViewHolder extends RecyclerView.ViewHolder {
        PostViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(PostPreview postPreview, final IListener listener) {
            GlideApp.with(itemView.getContext())
                    .load(postPreview.getAvatarUrl())
                    .dontTransform()
                    .dontAnimate()
                    .into(avatarImageView);
            titleTextView.setText(postPreview.getTitle());

            ViewCompat.setTransitionName(avatarImageView, postPreview.getId()+postPreview.getAvatarUrl());
            ViewCompat.setTransitionName(titleTextView, postPreview.getId()+postPreview.getTitle());

            itemView.setOnClickListener(view -> listener.onItemSelected(postPreview.getId(),
                    new Pair<>(avatarImageView, avatarImageView.getTransitionName()),
                    new Pair<>(titleTextView, titleTextView.getTransitionName())));
        }

        @BindView(R.id.AvatarImageView)
        ImageView avatarImageView;

        @BindView(R.id.TitleTextView)
        TextView titleTextView;
    }
}
