package com.emrebabur.postlorem.ui.postdetail;

import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.ui.base.Presenter;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by emrebabur on 10/09/2017.
 */

public class PostDetailPresenter extends Presenter<IPostDetailContract.IPostDetailView>
        implements IPostDetailContract.IPostDetailPresenter {

    private final String TAG = this.getClass().getSimpleName();

    IPostRepository postRepository;

    @Inject
    public PostDetailPresenter(Scheduler mainScheduler, Scheduler backgroundScheduler, IPostRepository postRepository) {
        super(mainScheduler, backgroundScheduler);
        this.postRepository = postRepository;
    }

    @Override
    protected void load(String... args) {
        String postId = args[0];

        if(postId == null) {
            throw new IllegalArgumentException("Post ID cannot be null");
        }

        getView().showLoading();
        getCompositeDisposable().add(
                postRepository.getPostDetail(postId)
                        .subscribeOn(getBackgroundScheduler())
                        .observeOn(getMainScheduler())
                        .subscribeWith(new DisposableSingleObserver<PostDetail>() {
                            @Override
                            public void onSuccess(PostDetail postDetail) {
                                getView().hideLoading();
                                getView().showPostDetail(postDetail);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                getView().showError();
                            }
                        })
        );
    }
}
