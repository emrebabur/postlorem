package com.emrebabur.postlorem.ui.postdetail;

import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.ui.base.IPresenter;
import com.emrebabur.postlorem.ui.base.IView;

/**
 * Created by emrebabur on 09/09/2017.
 */

public interface IPostDetailContract {

    interface IPostDetailView extends IView {
        void showPostDetail(PostDetail postDetail);
    }

    interface IPostDetailPresenter extends IPresenter<IPostDetailView> {
    }
}
