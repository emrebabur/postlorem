package com.emrebabur.postlorem.ui.postdetail;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.emrebabur.postlorem.PostloremApp;
import com.emrebabur.postlorem.R;
import com.emrebabur.postlorem.di.component.DaggerPresentationComponent;
import com.emrebabur.postlorem.di.module.LayoutManagerModule;
import com.emrebabur.postlorem.di.module.PresenterModule;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.util.GlideApp;
import com.emrebabur.postlorem.util.IntentConst;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostDetailActivity extends AppCompatActivity implements IPostDetailContract.IPostDetailView {

    private final String TAG = this.getClass().getSimpleName();
    private String postId;

    @Inject IPostDetailContract.IPostDetailPresenter presenter;

    @BindView(R.id.Toolbar)
    Toolbar toolbar;

    @BindView(R.id.AvatarImageView)
    ImageView avatarImageView;

    @BindView(R.id.LoadingView)
    ProgressBar loadingView;

    @BindView(R.id.TitleTextView)
    TextView titleTextView;

    @BindView(R.id.BodyTextView)
    TextView bodyTextView;

    @BindView(R.id.CommentCountTextView)
    TextView commentCountTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);
        supportPostponeEnterTransition();

        DaggerPresentationComponent.builder()
                .appComponent(((PostloremApp) getApplication()).getAppComponent())
                .presenterModule(new PresenterModule())
                .layoutManagerModule(new LayoutManagerModule(this))
                .build()
                .inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        postId = getIntent().getStringExtra(IntentConst.KEY_POST_ID);
        String avatarImageViewTransitionName = getIntent().getStringExtra(IntentConst.KEY_AVATAR_IMAGE_TRANSITION_NAME);
        String titleTextViewTransitionName = getIntent().getStringExtra(IntentConst.KEY_TITLE_TRANSITION_NAME);
        avatarImageView.setTransitionName(avatarImageViewTransitionName);
        titleTextView.setTransitionName(titleTextViewTransitionName);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.subscribe(this, postId);
    }

    @Override
    protected void onStop() {
        presenter.unsubscribe();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        supportFinishAfterTransition();
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showPostDetail(PostDetail postDetail) {
        toolbar.setTitle(postDetail.getUserName());
        GlideApp.with(this)
                .load(postDetail.getAvatarUrl())
                .dontTransform()
                .dontAnimate()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .into(avatarImageView);

        titleTextView.setText(postDetail.getTitle());
        bodyTextView.setText(postDetail.getBody());

        String commentCount = getResources().getQuantityString(R.plurals.comment_count, postDetail.getCommentCount(), postDetail.getCommentCount());
        commentCountTextView.setText(commentCount);
    }

    @Override
    public void showError() {

    }
}
