package com.emrebabur.postlorem.ui.base;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by emrebabur on 09/09/2017.
 */

public abstract class Presenter<VIEW extends IView> implements IPresenter<VIEW> {

    private VIEW view;
    private CompositeDisposable compositeDisposable;
    private Scheduler mainScheduler;
    private Scheduler backgroundScheduler;

    public Presenter(Scheduler mainScheduler, Scheduler backgroundScheduler) {
        this.mainScheduler = mainScheduler;
        this.backgroundScheduler = backgroundScheduler;
    }

    @Override
    public void subscribe(VIEW view, String... args) {
        this.view = view;
        load(args);
    }

    @Override
    public void unsubscribe() {
        compositeDisposable.dispose();
        this.view = null;
    }

    protected VIEW getView() {
        return view;
    }

    public CompositeDisposable getCompositeDisposable() {
        if(compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    public Scheduler getMainScheduler() {
        return mainScheduler;
    }

    public Scheduler getBackgroundScheduler() {
        return backgroundScheduler;
    }

    protected abstract void load(String... args);
}
