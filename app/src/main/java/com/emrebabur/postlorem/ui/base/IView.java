package com.emrebabur.postlorem.ui.base;

/**
 * Created by emrebabur on 09/09/2017.
 */

public interface IView {

    void showLoading();
    void hideLoading();
    void showError();
}
