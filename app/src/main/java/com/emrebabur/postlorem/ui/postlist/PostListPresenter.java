package com.emrebabur.postlorem.ui.postlist;

import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.ui.base.Presenter;
import com.emrebabur.postlorem.model.IPostRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Created by emrebabur on 09/09/2017.
 */

public class PostListPresenter extends Presenter<IPostListContract.IPostListView>
        implements IPostListContract.IPostListPresenter {

    private final String TAG = this.getClass().getSimpleName();

    IPostRepository postRepository;

    @Inject
    public PostListPresenter(Scheduler mainScheduler, Scheduler backgroundScheduler, IPostRepository postRepository) {
        super(mainScheduler, backgroundScheduler);
        this.postRepository = postRepository;
    }

    @Override
    protected void load(String... args) {
        getView().showLoading();
        getCompositeDisposable().add(
                postRepository.getPostPreviewList()
                        .subscribeOn(getBackgroundScheduler())
                        .observeOn(getMainScheduler())
                        .subscribeWith(new DisposableSingleObserver<List<PostPreview>>() {
                            @Override
                            public void onSuccess(List<PostPreview> postPreviewList) {
                                getView().hideLoading();
                                getView().showPosts(postPreviewList);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                getView().showError();
                            }
                        })
        );
    }

    @Override
    public void onPostClick(String postId) {
        getView().openPostDetail(postId);
    }
}
