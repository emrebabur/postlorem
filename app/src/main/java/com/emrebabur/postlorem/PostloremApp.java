package com.emrebabur.postlorem;

import android.app.Application;

import com.emrebabur.postlorem.di.component.AppComponent;
import com.emrebabur.postlorem.di.component.DaggerAppComponent;
import com.emrebabur.postlorem.di.module.AppModule;
import com.emrebabur.postlorem.di.module.NetworkModule;

import static com.emrebabur.postlorem.util.CommonConst.BASE_URL;

/**
 * Created by emrebabur on 10/09/2017.
 */

public class PostloremApp extends Application {

    private final String TAG = this.getClass().getSimpleName();

    private AppComponent appComponent;

    public AppComponent getAppComponent() {
        if(appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .networkModule(new NetworkModule(BASE_URL))
                    .build();
        }

        return appComponent;
    }
}
