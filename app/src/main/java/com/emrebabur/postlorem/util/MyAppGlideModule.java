package com.emrebabur.postlorem.util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by emrebabur on 12/09/2017.
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}