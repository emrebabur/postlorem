package com.emrebabur.postlorem.util;

/**
 * Created by emrebabur on 10/09/2017.
 */

public class IntentConst {
    private static final IntentConst ourInstance = new IntentConst();

    public static IntentConst getInstance() {
        return ourInstance;
    }

    private IntentConst() {
    }

    public static final String KEY_POST_ID = "post_id";
    public static final String KEY_AVATAR_IMAGE_TRANSITION_NAME = "avatar_transition";
    public static final String KEY_TITLE_TRANSITION_NAME = "title_transition";
}
