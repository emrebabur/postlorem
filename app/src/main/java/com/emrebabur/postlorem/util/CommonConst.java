package com.emrebabur.postlorem.util;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class CommonConst {
    private static final CommonConst ourInstance = new CommonConst();

    public static CommonConst getInstance() {
        return ourInstance;
    }

    private CommonConst() {
    }

    public static final String BASE_URL = "http://jsonplaceholder.typicode.com/";
    public static final long IN_MEMORY_CACHE_TIME = 1000 * 60 * 15; // 15 mins
}
