package com.emrebabur.postlorem.model.source;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import io.reactivex.Single;

import static com.emrebabur.postlorem.util.CommonConst.IN_MEMORY_CACHE_TIME;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class InMemoryCache implements ICache {

    private Date postListCacheDate;
    private Map<String, Post> postCacheMap;

    private Map<String, Date> userCacheDateMap;
    private Map<String, User> userCacheMap;

    private Map<String, Date> commentCountCacheDateMap;
    private Map<String, Integer> commentCountCacheMap;

    public InMemoryCache() {
        postCacheMap = new TreeMap<>();
        userCacheDateMap = new HashMap<>();
        userCacheMap = new HashMap<>();
        commentCountCacheDateMap = new HashMap<>();
        commentCountCacheMap = new HashMap<>();
    }

    @Override
    public Single<Post> getPost(String id) {

        if (postListCacheDate != null && postCacheMap.containsKey(id)) {
            Date now = new Date();
            long cacheAge = now.getTime() - postListCacheDate.getTime();
            if (cacheAge <= IN_MEMORY_CACHE_TIME) {
                return Single.just(postCacheMap.get(id));
            }
        }

        return null;
    }

    @Override
    public Single<List<Post>> getPostList() {

        if (postListCacheDate != null) {
            Date now = new Date();
            long cacheAge = now.getTime() - postListCacheDate.getTime();
            if (cacheAge <= IN_MEMORY_CACHE_TIME) {
                return Single.just(new ArrayList<>(postCacheMap.values()));
            }
        }

        return null;
    }

    @Override
    public void cachePostList(List<Post> postList) {
        postListCacheDate = new Date();
        postCacheMap.clear();
        for (Post post : postList) {
            postCacheMap.put(post.getId(), post);
        }
    }

    @Override
    public Single<User> getUser(String id) {

        if (userCacheDateMap.containsKey(id)) {
            Date now = new Date();
            long cacheAge = now.getTime() - userCacheDateMap.get(id).getTime();
            if (cacheAge <= IN_MEMORY_CACHE_TIME) {
                return Single.just(userCacheMap.get(id));
            }
        }

        return null;
    }

    @Override
    public void cacheUser(User user) {
        userCacheDateMap.put(user.getId(), new Date());
        userCacheMap.put(user.getId(), user);
    }

    @Override
    public Single<Integer> getCommentCount(String postId) {

        if(commentCountCacheDateMap.containsKey(postId)) {
            Date now = new Date();
            long cacheAge = now.getTime() - commentCountCacheDateMap.get(postId).getTime();
            if(cacheAge <= IN_MEMORY_CACHE_TIME) {
                return Single.just(commentCountCacheMap.get(postId));
            }
        }

        return null;
    }

    @Override
    public void cacheCommentCount(String postId, int count) {
        commentCountCacheDateMap.put(postId, new Date());
        commentCountCacheMap.put(postId, count);
    }
}
