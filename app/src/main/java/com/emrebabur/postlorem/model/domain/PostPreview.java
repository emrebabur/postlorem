package com.emrebabur.postlorem.model.domain;

/**
 * Created by emrebabur on 10/09/2017.
 */

public class PostPreview{

    private String id;
    private String title;
    private String avatarUrl;

    public PostPreview(String id, String title, String email) {
        this.id = id;
        this.title = title;
        this.avatarUrl = "https://api.adorable.io/avatars/" + email;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PostPreview) {
            PostPreview o = (PostPreview) obj;
            return id.equals(o.getId()) && title.equals(o.getTitle()) && avatarUrl.equals(o.getAvatarUrl());
        } else {
            return false;
        }
    }
}
