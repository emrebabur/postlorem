package com.emrebabur.postlorem.model.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by emrebabur on 10/09/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    private String id;
    private String email;
    private String name;

    public User() {
    }

    public User(String id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof User) {
            User o = (User) obj;
            return id.equals(o.getId())
                    && email.equals(o.getEmail())
                    && name.equals(o.getName());
        } else {
            return false;
        }
    }
}
