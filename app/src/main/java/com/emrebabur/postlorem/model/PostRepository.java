package com.emrebabur.postlorem.model;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.model.source.ICache;
import com.emrebabur.postlorem.model.source.ISource;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by emrebabur on 09/09/2017.
 */

public class PostRepository implements IPostRepository {

    private final String TAG = this.getClass().getSimpleName();

    private ISource apiSource;
    private ICache inMemoryCache;


    @Inject
    public PostRepository(ISource apiSource, ICache inMemoryCache) {
        this.apiSource = apiSource;
        this.inMemoryCache = inMemoryCache;
    }

    @Override
    public Single<List<PostPreview>> getPostPreviewList() {
        return getPostList()
                .flatMapObservable(Observable::fromIterable)
                .flatMap(post -> getUser(post.getUserId()).toObservable(),
                        (post, user) -> new PostPreview(post.getId(), post.getTitle(), user.getEmail()))
                .toList();
    }

    @Override
    public Single<PostDetail> getPostDetail(String postId) {
        return getPost(postId)
                .flatMap(post -> Single.zip(getUser(post.getUserId()),
                        getCommentCount(post.getId()),
                        (user, commentCount) -> new PostDetail(post.getTitle(), post.getBody(), user.getName(), user.getEmail(), commentCount)));
    }

    public Single<Post> getPost(String id) {
        Single<Post> observable = inMemoryCache.getPost(id);

        if(observable == null) {
            observable = apiSource.getPost(id);
        }

        return observable;
    }

    private Single<List<Post>> getPostList() {
        Single<List<Post>> observable = inMemoryCache.getPostList();

        if(observable == null) {
            observable = apiSource.getPostList().map(postList -> {
                inMemoryCache.cachePostList(postList);
                return postList;
            });
        }

        return observable.map(postList -> {
            Collections.sort(postList);
            return postList;
        });
    }

    private Single<User> getUser(String id) {
        Single<User> observable = inMemoryCache.getUser(id);

        if(observable == null) {
            observable = apiSource.getUser(id).map(user -> {
                inMemoryCache.cacheUser(user);
                return user;
            });
        }

        return observable;
    }

    private Single<Integer> getCommentCount(String postId) {
        Single<Integer> observable = inMemoryCache.getCommentCount(postId);

        if(observable == null) {
            observable = apiSource.getCommentCount(postId).map(commentCount -> {
                inMemoryCache.cacheCommentCount(postId, commentCount);
                return commentCount;
            });
        }

        return observable;
    }

}
