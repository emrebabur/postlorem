package com.emrebabur.postlorem.model.source;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by emrebabur on 11/09/2017.
 */

public interface ISource {
    Single<Post> getPost(String id);

    Single<List<Post>> getPostList();

    Single<User> getUser(String id);

    Single<Integer> getCommentCount(String postId);
}