package com.emrebabur.postlorem.model.source;

import com.emrebabur.postlorem.model.data.Comment;
import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class ApiSource implements ISource {

    Api api;

    public ApiSource(Retrofit retrofit) {
        this.api = retrofit.create(Api.class);
    }

    public Single<Post> getPost(String id) {
        return api.getPost(id);
    }

    public Single<List<Post>> getPostList() {
        return api.getPosts();
    }

    public Single<User> getUser(String id) {
        return api.getUser(id);
    }

    public Single<Integer> getCommentCount(String postId) {
        return api.getComments(postId).map(commentList -> commentList.size());
    }


    interface Api {
        @GET("posts")
        Single<List<Post>> getPosts();

        @GET("posts/{id}")
        Single<Post> getPost(@Path("id") String id);

        @GET("users/{id}")
        Single<User> getUser(@Path("id") String id);

        @GET("comments")
        Single<List<Comment>> getComments(@Query("postId") String postId);
    }
}
