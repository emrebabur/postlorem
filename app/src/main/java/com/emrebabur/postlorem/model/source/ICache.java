package com.emrebabur.postlorem.model.source;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;

import java.util.List;

/**
 * Created by emrebabur on 11/09/2017.
 */

public interface ICache extends ISource {
    void cachePostList(List<Post> postList);

    void cacheUser(User user);

    void cacheCommentCount(String postId, int count);
}
