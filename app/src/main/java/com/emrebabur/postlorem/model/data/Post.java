package com.emrebabur.postlorem.model.data;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by emrebabur on 10/09/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Post implements Comparable<Post> {

    private String id;
    private String userId;
    private String title;
    private String body;

    public Post() {
    }

    public Post(String id, String userId, String title, String body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Post) {
            Post o = (Post) obj;
            return id.equals(o.getId())
                    && userId.equals(o.getUserId())
                    && title.equals(o.getTitle())
                    && body.equals(o.getBody());
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@NonNull Post o) {
        return id.compareTo(o.getId());
    }
}
