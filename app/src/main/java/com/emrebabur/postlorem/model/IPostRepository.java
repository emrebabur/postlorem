package com.emrebabur.postlorem.model;

import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.model.domain.PostPreview;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by emrebabur on 09/09/2017.
 */

public interface IPostRepository {
    Single<List<PostPreview>> getPostPreviewList();
    Single<PostDetail> getPostDetail(String postId);
}
