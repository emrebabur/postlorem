package com.emrebabur.postlorem.model.domain;

/**
 * Created by emrebabur on 10/09/2017.
 */

public class PostDetail {

    private String title;
    private String body;
    private String userName;
    private int commentCount;
    private String avatarUrl;

    public PostDetail(String title, String body, String userName, String email, int commentCount) {
        this.title = title;
        this.body = body;
        this.userName = userName;
        this.avatarUrl = "https://api.adorable.io/avatars/" + email;
        this.commentCount = commentCount;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getUserName() {
        return userName;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PostDetail) {
            PostDetail o = (PostDetail) obj;
            return title.equals(o.getTitle())
                    && body.equals(o.getBody())
                    && userName.equals(o.getUserName())
                    && commentCount == o.getCommentCount()
                    && avatarUrl.equals(o.getAvatarUrl());
        } else {
            return false;
        }
    }
}
