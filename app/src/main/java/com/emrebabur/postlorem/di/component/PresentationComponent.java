package com.emrebabur.postlorem.di.component;

import com.emrebabur.postlorem.di.module.AdapterModule;
import com.emrebabur.postlorem.di.module.LayoutManagerModule;
import com.emrebabur.postlorem.di.module.PresenterModule;
import com.emrebabur.postlorem.di.scope.PerView;
import com.emrebabur.postlorem.ui.postdetail.PostDetailActivity;
import com.emrebabur.postlorem.ui.postlist.PostListActivity;

import dagger.Component;

/**
 * Created by emrebabur on 12/09/2017.
 */
@PerView
@Component(
        dependencies = {
                AppComponent.class
        },
        modules = {
                PresenterModule.class,
                AdapterModule.class,
                LayoutManagerModule.class
        }
)
public interface PresentationComponent {
    void inject(PostListActivity view);
    void inject(PostDetailActivity view);
}
