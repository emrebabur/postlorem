package com.emrebabur.postlorem.di.scope;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by emrebabur on 12/09/2017.
 */

@Scope
@Documented
@Retention(value= RetentionPolicy.RUNTIME)
public @interface PerView
{
}
