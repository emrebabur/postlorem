package com.emrebabur.postlorem.di.module;

import com.emrebabur.postlorem.di.scope.PerView;
import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.ui.postdetail.IPostDetailContract;
import com.emrebabur.postlorem.ui.postdetail.PostDetailPresenter;
import com.emrebabur.postlorem.ui.postlist.IPostListContract;
import com.emrebabur.postlorem.ui.postlist.PostListPresenter;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

/**
 * Created by emrebabur on 10/09/2017.
 */
@Module
public class PresenterModule {

    @Provides @PerView
    IPostListContract.IPostListPresenter providePostListPresenter(@Named("main") Scheduler mainScheduler,
                                                          @Named("background") Scheduler backgroundScheduler,
                                                          IPostRepository postRepository) {
        return new PostListPresenter(mainScheduler, backgroundScheduler, postRepository);
    }

    @Provides @PerView
    IPostDetailContract.IPostDetailPresenter providePostDetailPresenter(@Named("main") Scheduler mainScheduler,
                                                              @Named("background") Scheduler backgroundScheduler,
                                                              IPostRepository postRepository) {
        return new PostDetailPresenter(mainScheduler, backgroundScheduler, postRepository);
    }
}
