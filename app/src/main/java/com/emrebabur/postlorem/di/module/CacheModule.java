package com.emrebabur.postlorem.di.module;

import com.emrebabur.postlorem.model.source.ICache;
import com.emrebabur.postlorem.model.source.InMemoryCache;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrebabur on 10/09/2017.
 */
@Module
public class CacheModule {
    @Provides @Singleton @Named("in_memory")
    ICache provideInMemoryCache() {
        return new InMemoryCache();
    }
}
