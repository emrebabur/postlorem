package com.emrebabur.postlorem.di.module;

import com.emrebabur.postlorem.BuildConfig;
import com.emrebabur.postlorem.model.source.ApiSource;
import com.emrebabur.postlorem.model.source.ISource;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by emrebabur on 10/09/2017.
 */
@Module
public class NetworkModule {

    private String baseUrl;
    public static final int API_RETRY_COUNT = 3;
    public static final int NETWORK_CONNECTION_TIMEOUT = 30; // 30 minutes

    public NetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Singleton
    @Provides
    @Named("retryInterceptor")
    public Interceptor provideRetryInterceptor() {
        return chain -> {
            Request request = chain.request();
            Response response = null;
            IOException exception = null;

            int tryCount = 0;
            while (tryCount < API_RETRY_COUNT && (response == null || !response.isSuccessful())) {
                try {
                    response = chain.proceed(request);
                } catch (IOException e) {
                    exception = e;
                } finally {
                    tryCount++;
                }
            }

            if (response == null && exception != null)
                throw exception;

            return response;
        };
    }


    @Singleton
    @Provides
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return logging;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                     @Named("retryInterceptor") Interceptor retryInterceptor) {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .addInterceptor(retryInterceptor)
                .connectTimeout(NETWORK_CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(loggingInterceptor);
        }

        return clientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides @Singleton @Named("api")
    ISource provideApiSource(Retrofit retrofit) {
        return new ApiSource(retrofit);
    }
}
