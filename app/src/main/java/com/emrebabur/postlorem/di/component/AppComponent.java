package com.emrebabur.postlorem.di.component;

import com.emrebabur.postlorem.di.module.AppModule;
import com.emrebabur.postlorem.di.module.CacheModule;
import com.emrebabur.postlorem.di.module.ModelModule;
import com.emrebabur.postlorem.di.module.NetworkModule;
import com.emrebabur.postlorem.model.IPostRepository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;

/**
 * Created by emrebabur on 10/09/2017.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                CacheModule.class,
                NetworkModule.class,
                ModelModule.class,
        }
)
public interface AppComponent {
    IPostRepository postRepository();
    @Named("main") Scheduler mainScheduler();
    @Named("background") Scheduler backgroundScheduler();
}
