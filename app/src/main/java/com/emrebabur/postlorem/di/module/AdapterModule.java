package com.emrebabur.postlorem.di.module;

import com.emrebabur.postlorem.ui.adapter.PostPreviewAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrebabur on 12/09/2017.
 */
@Module
public class AdapterModule {

    @Provides
    PostPreviewAdapter providePostPreviewAdapter() {
        return new PostPreviewAdapter();
    }
}
