package com.emrebabur.postlorem.di.module;

import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.model.PostRepository;
import com.emrebabur.postlorem.model.source.ICache;
import com.emrebabur.postlorem.model.source.ISource;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrebabur on 10/09/2017.
 */
@Module
public class ModelModule {

    @Provides @Singleton
    IPostRepository providePostRepository(@Named("api") ISource apiSource, @Named("in_memory") ICache inMemoryCache) {
        return new PostRepository(apiSource, inMemoryCache);
    }
}
