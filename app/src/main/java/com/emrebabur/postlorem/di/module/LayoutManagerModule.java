package com.emrebabur.postlorem.di.module;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.emrebabur.postlorem.di.scope.PerView;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emrebabur on 12/09/2017.
 */
@Module
public class LayoutManagerModule {

    Context context;

    public LayoutManagerModule(Context context) {
        this.context = context;
    }

    @Provides @PerView @Named("vertical")
    LinearLayoutManager provideVerticalLinearLayoutManager() {
        return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
    }

    @Provides @PerView @Named("horizontal")
    LinearLayoutManager provideHorizontalLinearLayoutManager() {
        return new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
    }

}
