package com.emrebabur.postlorem;

import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.ui.postlist.IPostListContract;
import com.emrebabur.postlorem.ui.postlist.PostListPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class PostListPresenterTest {

    @Mock IPostRepository postRepository;

    @Mock
    IPostListContract.IPostListView view;

    PostListPresenter presenter;
    List<PostPreview> postPreviewList = MockModelGenerator.getInstance().getPostPreviewList();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Scheduler immediate = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        presenter = new PostListPresenter(Schedulers.single(), Schedulers.single(), postRepository);


        when(postRepository.getPostPreviewList()).thenReturn(Single.just(postPreviewList));
        presenter.subscribe(view);
    }

    @Test public void shouldCallShowLoading() {
        verify(view).showLoading();
    }

    @Test public void shouldUpdateView() {
        verify(view).hideLoading();
        verify(view).showPosts(postPreviewList);
        verify(view, times(0)).showError();
    }
}
