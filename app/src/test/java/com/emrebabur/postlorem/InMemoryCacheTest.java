package com.emrebabur.postlorem;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;
import com.emrebabur.postlorem.model.source.ICache;
import com.emrebabur.postlorem.model.source.InMemoryCache;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.verify;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class InMemoryCacheTest {

    ICache inMemoryCache;

    int postOrder = 0;
    int userOrder = 0;
    int commentCount = 10;

    List<Post> postList = MockModelGenerator.getInstance().getPostList();
    User user = MockModelGenerator.getInstance().getUserList().get(userOrder);

    @Before
    public void setUp() throws Exception {
        inMemoryCache = new InMemoryCache();
    }

    @Test
    public void shouldCacheThenReturnPostList() {
        inMemoryCache.cachePostList(postList);
        inMemoryCache.getPostList()
                .test()
                .assertResult(postList);
    }

    @Test
    public void shouldCacheThenReturnUser() {
        inMemoryCache.cacheUser(user);
        inMemoryCache.getUser(String.valueOf(userOrder+1))
                .test()
                .assertResult(user);
    }

    @Test
    public void shouldCacheThenReturnCommentCount() {
        inMemoryCache.cacheCommentCount(String.valueOf(postOrder), commentCount);
        inMemoryCache.getCommentCount(String.valueOf(postOrder))
                .test()
                .assertResult(commentCount);
    }
}
