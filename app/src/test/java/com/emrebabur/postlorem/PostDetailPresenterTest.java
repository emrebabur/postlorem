package com.emrebabur.postlorem;

import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.ui.postdetail.IPostDetailContract;
import com.emrebabur.postlorem.ui.postdetail.PostDetailPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class PostDetailPresenterTest {

    @Mock
    IPostRepository postRepository;

    @Mock
    IPostDetailContract.IPostDetailView view;

    PostDetailPresenter presenter;

    PostDetail postDetail = MockModelGenerator.getInstance().getPostDetailList().get(0);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Scheduler immediate = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        presenter = new PostDetailPresenter(Schedulers.single(), Schedulers.single(), postRepository);

        when(postRepository.getPostDetail(anyString())).thenReturn(Single.just(postDetail));
        presenter.subscribe(view, "");
    }

    @Test
    public void shouldCallShowLoading() {
        verify(view).showLoading();
    }

    @Test public void shouldUpdateView() {
        verify(view).hideLoading();
        verify(view).showPostDetail(postDetail);
        verify(view, times(0)).showError();
    }
}
