package com.emrebabur.postlorem;

import com.emrebabur.postlorem.model.IPostRepository;
import com.emrebabur.postlorem.model.PostRepository;
import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.model.domain.PostPreview;
import com.emrebabur.postlorem.model.source.ICache;
import com.emrebabur.postlorem.model.source.ISource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class PostRepositoryTest {
    @Mock ISource apiSource;
    @Mock
    ICache inMemoryCache;

    IPostRepository repository;

    int postOrder = 0;
    int userOrder = 0;
    int commentCount = 10;

    List<Post> postList = MockModelGenerator.getInstance().getPostList();
    List<User> userList = MockModelGenerator.getInstance().getUserList();
    List<PostPreview> postPreviewList = MockModelGenerator.getInstance().getPostPreviewList();
    PostDetail postDetail = MockModelGenerator.getInstance().getPostDetailList().get(0);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        repository = new PostRepository(apiSource, inMemoryCache);
    }

    @Test
    public void shouldReturnPostPreviewListFromApi() {
        when(apiSource.getPostList()).thenReturn(Single.just(postList));
        when(apiSource.getUser(anyString())).thenReturn(Single.just(userList.get(userOrder)));

        repository.getPostPreviewList()
                .test()
                .assertResult(postPreviewList);

        verify(inMemoryCache).getPostList();
        verify(apiSource).getPostList();
        verify(inMemoryCache).cachePostList(postList);
        verify(inMemoryCache).getUser(anyString());
        verify(apiSource).getUser(anyString());
        verify(inMemoryCache).cacheUser(userList.get(userOrder));

    }

    @Test
    public void shouldReturnPostPreviewListFromCache() {
        when(inMemoryCache.getPostList()).thenReturn(Single.just(postList));
        when(inMemoryCache.getUser(anyString())).thenReturn(Single.just(userList.get(userOrder)));

        repository.getPostPreviewList()
                .test()
                .assertResult(postPreviewList);

        verify(inMemoryCache).getPostList();
        verify(apiSource, times(0)).getPostList();
        verify(inMemoryCache, times(0)).cachePostList(postList);
        verify(inMemoryCache).getUser(anyString());
        verify(apiSource, times(0)).getUser(anyString());
        verify(inMemoryCache, times(0)).cacheUser(userList.get(userOrder));

    }

    @Test
    public void shoultReturnPostDetailFromApi() {
        when(apiSource.getPost(anyString())).thenReturn(Single.just(postList.get(postOrder)));
        when(apiSource.getUser(anyString())).thenReturn(Single.just(userList.get(userOrder)));
        when(apiSource.getCommentCount(anyString())).thenReturn(Single.just(commentCount));

        repository.getPostDetail(anyString())
                .test()
                .assertResult(postDetail);

        verify(inMemoryCache).getPost(anyString());
        verify(apiSource).getPost(anyString());
        verify(inMemoryCache).getUser(anyString());
        verify(apiSource).getUser(anyString());
        verify(inMemoryCache).cacheUser(userList.get(userOrder));
        verify(inMemoryCache).getCommentCount(anyString());
        verify(apiSource).getCommentCount(anyString());
    }

    @Test
    public void shoultReturnPostDetailFromCache() {
        when(inMemoryCache.getPost(anyString())).thenReturn(Single.just(postList.get(postOrder)));
        when(inMemoryCache.getUser(anyString())).thenReturn(Single.just(userList.get(userOrder)));
        when(inMemoryCache.getCommentCount(anyString())).thenReturn(Single.just(commentCount));

        repository.getPostDetail(anyString())
                .test()
                .assertResult(postDetail);

        verify(inMemoryCache).getPost(anyString());
        verify(apiSource, times(0)).getPost(anyString());
        verify(inMemoryCache).getUser(anyString());
        verify(apiSource, times(0)).getUser(anyString());
        verify(inMemoryCache, times(0)).cacheUser(userList.get(userOrder));
        verify(inMemoryCache).getCommentCount(anyString());
        verify(apiSource, times(0)).getCommentCount(anyString());
    }

}
