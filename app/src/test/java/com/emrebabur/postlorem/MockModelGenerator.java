package com.emrebabur.postlorem;

import com.emrebabur.postlorem.model.data.Post;
import com.emrebabur.postlorem.model.data.User;
import com.emrebabur.postlorem.model.domain.PostDetail;
import com.emrebabur.postlorem.model.domain.PostPreview;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emrebabur on 11/09/2017.
 */

public class MockModelGenerator {
    private static MockModelGenerator ourInstance;

    public static MockModelGenerator getInstance() {
        if(ourInstance == null) {
            ourInstance = new MockModelGenerator();
        }
        return ourInstance;
    }


    private static final List<Post> postList = new ArrayList<>();
    private static final List<User> userList = new ArrayList<>();
    private static final List<PostPreview> postPreviewList = new ArrayList<>();
    private static final List<PostDetail> postDetailList = new ArrayList<>();

    private MockModelGenerator() {

        for(int i=0; i<1; i++) {
            postList.add(new Post(String.valueOf(i+1), String.valueOf(i/10 + 1), "Title", "Body"));
            userList.add(new User(String.valueOf(i+1), "User" + (i+1), "User" + (i+1) + "@mail.com"));

            Post post = postList.get(i);
            User user = userList.get(i);
            postPreviewList.add(new PostPreview(post.getId(), post.getTitle(), user.getEmail()));
            postDetailList.add(new PostDetail(post.getTitle(), post.getBody(), user.getName(), user.getEmail(), 10));
        }
    }

    public static List<Post> getPostList() {
        return postList;
    }

    public static List<User> getUserList() {
        return userList;
    }

    public static List<PostPreview> getPostPreviewList() {
        return postPreviewList;
    }

    public static List<PostDetail> getPostDetailList() {
        return postDetailList;
    }
}
